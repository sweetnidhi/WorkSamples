package resources;

public class Book {
    private String id;
    private String title;
    private String author;
    private Info1 info;
    public Book()
    {
    }
    public  Book(String id, String title, String author, String email, String phone, String address)
    {
        this.id=id;
        this.title=title;
        this.author=author;
        info = new Info1(email,phone,address);
    }

    public  Book(String id, String title, String author, Info1 info1)
    {
        this.id=id;
        this.title=title;
        this.author=author;

        //info = new Info(info1.getPhone(),info1.getEmail(),info1.getAddress());
        info = info1;
    }
    public  Book(String title,String author)
    {
        this.title=title;
        this.author=author;
        this.id="1000";
    }
    public  Book(String id,String title,String author)
    {
        this.title=title;
        this.author=author;
        this.id=id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Info1 getInfo() {
        return info;
    }

    public void setInfo(Info1 info) {
        this.info = info;
    }

    public String getId(){
        return id;
    }

    public void setID(String id)
    {
        this.id=id;
    }
    public String getTitle()
    {
        return  title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }
    public String getAuthor()
    {
        return author;
    }
    public void setAuthor(String author)
    {
        this.author = author;
    }

}

