package resources;

public class Info1 {
    private String phone;
    private String email;
    private String address;

    public Info1(String email,String phone,String address){
        this.phone = phone;
        this.address = address;
        this.email = email;

    }
    public Info1()
    {}

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

        public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
