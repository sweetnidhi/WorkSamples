package testScripts;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Test;
import resources.Book;
import resources.Info1;

import java.io.FileInputStream;
import java.util.Random;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BookApiTests {
    private static final String BASE_URL = "http://localhost:3000";

    @Test
    public void simpleGet() {
        Response resp = given().
                when().
                get(BASE_URL + "/posts");
        assertThat(resp.statusCode(),equalTo(200));
        System.out.println("Response:"+resp.asString());
    }

    @Test
    public void postAsString() {
        Response resp = given().
                body("{ \"id\" : 2, \"title\": \"ladoo\", \"author\": \"meenu\"}").
                when().
                contentType(ContentType.JSON).
                post(BASE_URL + "/posts");
        assertThat(resp.statusCode(),equalTo(201));
        System.out.println("Response:"+resp.asString());
    }

    @Test
    public void postAsObject() {
        Book b = new Book("100", "Nag the Great", "Mohit");
        b.setID(String.valueOf(new Random().nextInt(1090)));
        Response resp1 = given()
                .when()
                .headers("correlation-id",UUID.randomUUID())
                .contentType(ContentType.JSON)
                .body(b)
                .post(BASE_URL + "/posts");
        System.out.println("Response:"+"Response:" + resp1.asString());
        assertThat(resp1.statusCode(),equalTo(201));

        b = new Book();
        b.setID(String.valueOf(new Random().nextInt(1221)));
        b.setTitle("great Nag11");
        b.setAuthor("Mohit111");


        System.out.println("Response:"+b.getId());
        Response resp3 = given().
                when().
                contentType(ContentType.JSON).
                body(b).
                post(BASE_URL + "/posts");
        System.out.println("Response:"+resp3.asString());

        assertThat(resp3.statusCode(),equalTo(201));

        b.setID(String.valueOf(new Random().nextInt(1000)));

        b.setTitle("nag the bad guy");
        b.setAuthor("shaurya");

        Response resp2 = given().
                when().
                contentType(ContentType.JSON).
                body(b).
                post(BASE_URL + "/posts");
        assertThat(resp2.statusCode(),equalTo(201));
        System.out.println("Response:"+resp2.body().path("id"));
        assertThat(resp2.body().path("id"),notNullValue());
        System.out.println("Response:"+resp2.asString());
    }

    @Test
    public void patchTest() {
        given()
                .body("{ \"author\": \"Nelson\"}")
                .when()
                .contentType(ContentType.JSON)
                .patch(BASE_URL +"/posts/292")
                .then()
                .statusCode(200);;
    }

    @Test
    public void deleteBook() {
        int id = given()
                .contentType(ContentType.JSON)
                .body(getInputFileAsString())
                .when()
                .post(BASE_URL+"/posts")
                .then()
                .extract()
                .path("id");

        given()
                .contentType(ContentType.JSON)
                .when()
                .delete(BASE_URL + "/posts/"+ String.valueOf(id))
                .then()
                .statusCode(200);
    }

    @Test
    public void complexPost() {
        Info1 info = new Info1("veer.mathur@info.com", "99999", "kuch to bhi");
        Book book = new Book("1006", "veer the great", "mohit", info);
        given()
                .body(book)
                .when()
                .contentType(ContentType.JSON)
                .post(BASE_URL + "/posts")
                .then()
                .statusCode(201);
    }

    @Test
    public void iCheckForTheBookWithIdForEmail() {
        String email = "veer.mathur@info.com";
        String id = "1006";
        given()
                .when()
                .get(BASE_URL + "/posts/{0}", id)
                .then()
                .body("info.email",equalTo(email));
    }

    @Test
    public void iPostBookDetailsFromFile() {
            given()
                    .contentType(ContentType.JSON)
                    .body(getInputFileAsString())
                    .when()
                    .post(BASE_URL+ "/posts")
                    .then().statusCode(201);
    }

    @Test
    public void getAllBooksWithTitleJsonServer() {
        try {
            given()
                    .when()
                    .get(BASE_URL + "/posts")
                    .then()
                    .body("title", hasItem("json-server"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
  private String getInputFileAsString() {
      String file = System.getProperty("user.dir") + "/resourceFiles/bookPost.json";
      try {
          FileInputStream f = new FileInputStream(file);
          byte[] b = new byte[1000];
          f.read(b);
          String req = new String(b);
          req = req.replace("DYNAMIC", String.valueOf(new Random().nextInt(99)));
          return req.trim();
      }catch (Exception e) {
          System.out.println("Response:"+"exception received"+e);
      }
      return null;
  }
}
