package testScripts;


import static io.restassured.RestAssured.given;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;

public class WeatherTests {
    @Test
    public void getTest()
    {
        Response resp = given().
                        param("q","London").
                        param("appid","b6907d289e10d714a6e88b30761fae22").
                        when().
                        get("http://samples.openweathermap.org/data/2.5/weather");
        System.out.println(resp.getStatusCode());
    }

    @Test
    public void getTestWithAssert()
    {
                given().
                param("q","London").
                param("appid","b6907d289e10d714a6e88b30761fae22").
                when().
                get("http://samples.openweathermap.org/data/2.5/weather").
                then().
                assertThat().statusCode(200);
    }

    @Test
    public void getTestWithChecks()
    {
        Response resp = given().
                param("q","London").
                param("appid","b6907d289e10d714a6e88b30761fae22").
                when().
                get("http://samples.openweathermap.org/data/2.5/weather");
        String actualWeatherReport = resp.then().
                contentType(ContentType.JSON).
                extract().
                path("weather[0].description");
        String expectedWeatherReport = "light intensity drizzle";
        assertThat("",actualWeatherReport.contains(expectedWeatherReport));
        if(actualWeatherReport.equalsIgnoreCase(expectedWeatherReport))
            System.out.println("great its a pass");

        System.out.println(resp.asString());
    }
    @Test
    public void getTestWithResultFromOtherGet()
    {
        Response resp = given().
                param("id","2172797").
                param("appid","b6907d289e10d714a6e88b30761fae22").
                when().
                get("http://samples.openweathermap.org/data/2.5/weather");
        String reportbyCity = resp.then().
                contentType(ContentType.JSON).
                extract().
                path("weather[0].description");
        String lat = String.valueOf(resp.then().
                contentType(ContentType.JSON).
                extract().
                path("coord.lat"));
        String lon = String.valueOf(resp.then().
                contentType(ContentType.JSON).
                extract().
                path("coord.lon"));
        System.out.println(lat + " lon:" + lon);
        Response resp2 = given().
                param("lat",lat).
                param("lon",lon).
                param("appid","b6907d289e10d714a6e88b30761fae22").
                when().
                get("http://samples.openweathermap.org/data/2.5/weather");
        String reportbyCoord = resp2.then().
                contentType(ContentType.JSON).
                extract().
                path("weather[0].description");
        Assert.assertEquals(reportbyCity,reportbyCoord);
    }
}
