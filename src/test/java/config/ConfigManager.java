package config;

import java.io.IOException;
import java.util.Properties;

public class ConfigManager {
    private Properties properties;
    //String final CONFIG_PATH = System.getProperty("user.dir") + "/src/test/java/resorces/";
    public ConfigManager() {
        String file =  "config.properties";
         properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
