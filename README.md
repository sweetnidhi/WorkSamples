This is a simple project to work with RestAssured to test some REST APIs.

WeatherTests
Used the open Weather API to test the GET requests to get weather information of a 
particular city => testScripts/WeatherTests.java

The API is available at https://openweathermap.org/api

To run these tests:
1. Open this project in an IDE.
2. Select WeatherTests and click on Run

BookApiTests
Use a local json server and try POST,PUT,DELETE APIs to store/get info into Book(db.json) database.
=> testScripts/BookApiTests.java - contains all the tests to verify the APIs using GET, POST, PUT, DELETE.
   Uses resources/Book.java, resources/Info.java - Class definition of the format used in the JSON body
=> Database files are available at resourceFiles folder at the top of directory. 
db.json is the db and bookPost.json is a sample json file used in post test.
=> Local json server used: https://github.com/typicode/json-server

To run these tests:
1. Download the json-server from https://github.com/typicode/json-server and install using "npm install -g json-server"
2. In Terminal, run the json-server on your local machine using command "json-server --watch db.json --port 3000". 
3. The db.json given here is present in resourceFiles folder of this project
4. To run the tests, open this project in an IDE and select BookApiTests and click on Run.
